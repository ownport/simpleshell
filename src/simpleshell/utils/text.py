
from typing import Any


def to_text(obj:Any, encoding:str='utf-8', errors:str='strict') -> str:
    """ Make sure that a string is a text string
    
    - obj: An object to make sure is a text string
    - encoding: The encoding to use to transform from a byte string to a text string.  
            Default: 'utf-8'.
    - errors: The error handler to use if the byte string is not decodable using 
            the specified encoding "https://docs.python.org/3/library/codecs.html#error-handlers"
            Default: 'strict'
    
    returns: Typically this returns a text string
    """
    if not isinstance(obj, (str, bytes)):
        raise TypeError(f'Unsupported type: {type(obj)}, support: (str, bytes)')

    if isinstance(obj, str):
        return obj

    return obj.decode(encoding, errors)

