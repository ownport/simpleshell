
import json
import yaml
import logging

try:
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader


logger = logging.getLogger(__name__)


class Settings:

    def __init__(self) -> None:
        
        self._settings = dict()


    def load(self, *vars):
        ''' reused from https://github.com/ansible/ansible/blob/devel/lib/ansible/utils/vars.py
            and modified according to simpleshell requirements
        '''
        for _vars in vars:
            data = None
            if _vars.startswith(u"@"):
                # Argument is a YAML file (JSON is a subset of YAML)
                try:
                    with open(_vars[1:], 'r', encoding='utf8') as source:
                        try:
                            data = yaml.load(source, Loader=Loader)
                        except yaml.YAMLError as err:
                            logger.error('%s, %s', err, _vars)
                except FileNotFoundError as err:
                    logger.error(err)
            else:
                try:
                    data = json.loads(_vars)
                except json.JSONDecodeError as err:
                    logger.error('%s, %s', err, _vars)

            if not data:
                logger.warning(f'No variables found, vars: {_vars}')
                continue
            
            if not isinstance(data, dict):
                logger.warning(f'Incorrect variables format, expected: dict, found: {type(_vars)}')
                continue

            self._settings.update(data)

