
import logging

from simpleshell.shell import c

logger = logging.getLogger(__name__)


if __name__ == '__main__':

    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument('--command', type=str, required=True,
                        help="Command for executing")
    parser.add_argument('--console-output', type=bool, 
                        nargs='?', const=True, default=False,
                        help="Output to console")
    parser.add_argument('--log-level', default='INFO',
                        help='Logging level, defaut: INFO')
    args = parser.parse_args()

    logging.basicConfig(level=args.log_level, datefmt='%Y-%m-%dT%H:%M:%S',
                        format="%(asctime)s.%(msecs)03d (%(name)s) [%(levelname)s] %(message)s")

    rc, stdout, stderr = c(args.command, console=args.console_output).run()
    logger.info(f'Return Code: {rc}')
    logger.info(f'Stdout: {stdout}')
    logger.info(f'Stderr: {stderr}')
