
import os
import jinja2
import logging

logger = logging.getLogger(__name__)


class Template:

    def __init__(self) -> None:
        
        self._template = None

    def parse(self, template_path:str, vars:dict):
        ''' parse template
        '''
        path = os.path.dirname(template_path)
        filename = os.path.basename(template_path)

        template = jinja2.Environment(
                        loader=jinja2.FileSystemLoader(path),
                        undefined=jinja2.StrictUndefined
                    ).get_template(filename)

        try:
            self._template = template.render(**vars)
        except jinja2.exceptions.UndefinedError as err:
            logger.error(f'Templating, {err}')

