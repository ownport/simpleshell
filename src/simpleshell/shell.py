# 
#  Based on https://github.com/ansible/ansible/blob/devel/lib/ansible/module_utils/basic.py
# 
import os
import sys
import fcntl
import shlex
import typing
import logging
import traceback
import selectors
import subprocess

from simpleshell.utils.text import to_text


logger = logging.getLogger(__name__)

# Types
ReturnCode = int
StdOutout = bytes
StdError = bytes

CommandOutput = (ReturnCode, StdOutout, StdError)

class Console:
    ''' Console output
    '''
    def put(self, data:bytes, datatype:str='stdout') -> typing.NoReturn:
        ''' put data into console
        '''
        data = to_text(data)
        if datatype == 'stdout':
            sys.stdout.write(data)
        else:
            sys.stderr.write(data)

class Command:
    ''' Shell Command
    '''
    def __init__(self, cmd:str, cwd:str=None, console:bool=False) -> typing.NoReturn:
        ''' Execute a command, returns rc, stdout, and stderr.
        
        :arg cmd: is the command to run
        :arg cwd: If given, working directory to run the command inside
        :arg console: print stdout/stderr to console if True
        '''
        # handling command arguments
        if not isinstance(cmd, str):
            _msg = f"Incorrect parameter type for `command`, expected: str, detedcted, {type(cmd)}, command: {cmd}"
            logger.error(_msg)
            raise TypeError(_msg)

        self._args = [os.path.expanduser(os.path.expandvars(x))
                            for x in shlex.split(cmd) if x is not None]

        # activate console output
        if not isinstance(console, bool):
            _msg = f'Incorrect parameter type for `console`, expected: bool, detected: {type(console)}'
            logger.error(_msg)
            raise TypeError(_msg)
        self._console = Console() if console else None

        # store the pwd
        self._prev_dir = os.getcwd()

        # setting kwargs 
        self._kwargs = dict( stdout=subprocess.PIPE, stderr=subprocess.PIPE, )

        # change working directory
        if cwd:
            cwd = os.path.abspath(os.path.expanduser(cwd))
            if os.path.isdir(cwd):
                self._kwargs['cwd'] = cwd
                try:
                    os.chdir(cwd)
                except (OSError, IOError) as e:
                    error_msg = self._error_msg( rc=e.errno, msg="Could not chdir to %s, %s" % (cwd, e),
                                   exception=traceback.format_exc())
                    logger.error(error_msg)
            else:
                logger.error(f'The path to working directory does not exist, {cwd}')

        try:
            self._selector = selectors.DefaultSelector()
        except (IOError, OSError):
            # Failed to detect default selector for the given platform
            # Select PollSelector which is supported by major platforms
            self._selector = selectors.PollSelector()

    def run(self) -> CommandOutput:
        ''' run shell command
        '''
        # Return Code
        rc = 0
        stdout = b''
        stderr = b''

        try:
            cmd = subprocess.Popen(self._args, **self._kwargs)

            self._selector.register(cmd.stdout, selectors.EVENT_READ)
            self._selector.register(cmd.stderr, selectors.EVENT_READ)
            fcntl.fcntl(cmd.stdout.fileno(), 
                        fcntl.F_SETFL, 
                        fcntl.fcntl(cmd.stdout.fileno(), fcntl.F_GETFL) | os.O_NONBLOCK)
            fcntl.fcntl(cmd.stderr.fileno(), 
                        fcntl.F_SETFL, 
                        fcntl.fcntl(cmd.stderr.fileno(), fcntl.F_GETFL) | os.O_NONBLOCK)

            while True:
                events = self._selector.select(-1)

                for key, event in events:
                    b_chunk = key.fileobj.read()
                    if b_chunk == b'':
                        self._selector.unregister(key.fileobj)
                    if key.fileobj == cmd.stdout:
                        stdout += b_chunk
                        if self._console:
                            self._console.put(b_chunk, datatype='stdout')
                    elif key.fileobj == cmd.stderr:
                        stderr += b_chunk
                        if self._console:
                            self._console.put(b_chunk, datatype='stderr')
                
                # only break out if no pipes are left to read or
                # the pipes are completely read and
                # the process is terminated
                if (not events or not self._selector.get_map()) and cmd.poll() is not None:
                    break
                # No pipes are left to read but process is not yet terminated
                # Only then it is safe to wait for the process to be finished
                # NOTE: Actually cmd.poll() is always None here if no selectors are left
                elif not self._selector.get_map() and cmd.poll() is None:
                    cmd.wait()
                    # The process is terminated. Since no pipes to read from are
                    # left, there is no need to call select() again.
                    break

            cmd.stdout.close()
            cmd.stderr.close()
            self._selector.close()

            rc = cmd.returncode

        except (OSError, IOError) as e:
            error_msg = self._error_msg(rc=e.errno, msg=e, cmd=self._args)
            logger.error(f"Cannot execute command: {error_msg}")
        except Exception as e:
            error_msg = self._error_msg(rc=257, msg=e, exception=traceback.format_exc(), 
                                        cmd=self._args)
            logger.error(f"Cannot execute command: {error_msg}")

        # reset the pwd
        os.chdir(self._prev_dir)

        return (rc, stdout, stderr)

    @staticmethod
    def _error_msg(**kwargs) -> dict:
        ''' returns error details as dict
        '''
        return kwargs


c = Command
